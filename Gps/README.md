![](https://img.shields.io/badge/Java-ED8B00?style=for-the-badge&logo=java&logoColor=white)
![](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)

[[_TOC_]]

# Gps _services_ application

## Description

The application is a part of **the main [tourGuide application](https://gitlab.com/abdoum/DA_Java_P8/-/tree/develop)**
it aims to simulate a microservice managing the gps service for the main application.

## Requirements

----

- Unix or Windows OS
- [Java](https://www.java.com/), [Apache](https://httpd.apache.org/docs/) and [Gradle](https://gradle.org/) Wrapper

## Installation and Getting Started

----
**To run locally:**

1. Clone this repo to your machine and run using the following commands.

```shell
$ git clone https://gitlab.com/abdoum/DA_Java_P8/-/tree/develop
$ cd DA_Java_P8/Gps
```

- Mac OS X/*nix

```shell
$ ./gradlew build
$ ./gradlew bootRun
```

- Windows

```shell
gradlew.bat build
gradlew.bat bootRun
```

2. Once started go
   to [http://localhost:9002/swagger-ui.html#/gps45controller](http://localhost:9002/swagger-ui.html#/gps45controller)
   to and try the available endpoints.
3. Finally, start the other [services](https://gitlab.com/abdoum/DA_Java_P8/-/tree/develop) to see them interacting with
   each others.

## Test reports

Test [coverage report](build/jacocoHtml/index.html)

## Javadoc

Link to [javadoc](build/docs/javadoc/index-all.html)

---

![uml diagram](src/main/resources/static/uml_diagram.png)