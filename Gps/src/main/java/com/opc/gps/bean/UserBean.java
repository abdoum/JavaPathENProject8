package com.opc.gps.bean;


import java.util.*;

public class UserBean {

    private final UUID userId;

    private final String userName;

    private String phoneNumber;

    private String emailAddress;

    private Date latestLocationTimestamp;

    private final List<VisitedLocationBean> visitedLocations = new ArrayList<>();

    private final List<UserRewardBean> userRewards = new ArrayList<>();

    private UserPreferences userPreferences = new UserPreferences();

    private List<ProviderBean> tripDeals = new ArrayList<>();

    /**
     * Instantiates a new User bean.
     *
     * @param userId       the user id
     * @param userName     the user name
     * @param phoneNumber  the phone number
     * @param emailAddress the email address
     */
    public UserBean(UUID userId, String userName, String phoneNumber, String emailAddress) {
        this.userId       = userId;
        this.userName     = userName;
        this.phoneNumber  = phoneNumber;
        this.emailAddress = emailAddress;
    }

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public UUID getUserId() {
        return userId;
    }

    /**
     * Gets user name.
     *
     * @return the user name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets phone number.
     *
     * @param phoneNumber the phone number
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Gets phone number.
     *
     * @return the phone number
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets email address.
     *
     * @param emailAddress the email address
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * Gets email address.
     *
     * @return the email address
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets latest location timestamp.
     *
     * @param latestLocationTimestamp the latest location timestamp
     */
    public void setLatestLocationTimestamp(Date latestLocationTimestamp) {
        this.latestLocationTimestamp = latestLocationTimestamp;
    }

    /**
     * Gets latest location timestamp.
     *
     * @return the latest location timestamp
     */
    public Date getLatestLocationTimestamp() {
        return latestLocationTimestamp;
    }

    /**
     * Add to visited locations.
     *
     * @param visitedLocation the visited location
     */
    public void addToVisitedLocations(VisitedLocationBean visitedLocation) {
        visitedLocations.add(visitedLocation);
    }

    /**
     * Gets visited locations.
     *
     * @return the visited locations
     */
    public List<VisitedLocationBean> getVisitedLocations() {
        return visitedLocations;
    }

    /**
     * Clear visited locations.
     */
    public void clearVisitedLocations() {
        visitedLocations.clear();
    }

    /**
     * Add user reward.
     *
     * @param userRewardBean the user reward bean
     */
    public void addUserReward(UserRewardBean userRewardBean) {
        if (userRewards.stream()
                       .noneMatch(reward -> reward.attraction.attractionName.equals(
                                   userRewardBean.attraction.attractionName))) {
            userRewards.add(userRewardBean);
        }
    }

    /**
     * Gets user rewards.
     *
     * @return the user rewards
     */
    public List<UserRewardBean> getUserRewards() {
        return userRewards;
    }

    /**
     * Gets user preferences.
     *
     * @return the user preferences
     */
    public UserPreferences getUserPreferences() {
        return userPreferences;
    }

    /**
     * Sets user preferences.
     *
     * @param userPreferences the user preferences
     */
    public void setUserPreferences(UserPreferences userPreferences) {
        this.userPreferences = userPreferences;
    }

    /**
     * Gets last visited location.
     *
     * @return the last visited location
     */
    public VisitedLocationBean getLastVisitedLocation() {
        return visitedLocations.get(visitedLocations.size() - 1);
    }

    /**
     * Gets last visited location coordinates.
     *
     * @return the last visited location coordinates
     */
    public LocationBean getLastVisitedLocationCoordinates() {
        return visitedLocations.get(visitedLocations.size() - 1).location;
    }

    /**
     * Sets trip deals.
     *
     * @param tripDeals the trip deals
     */
    public void setTripDeals(List<ProviderBean> tripDeals) {
        this.tripDeals = tripDeals;
    }

    /**
     * Gets trip deals.
     *
     * @return the trip deals
     */
    public List<ProviderBean> getTripDeals() {
        return tripDeals;
    }

    /**
     * Gets location to string.
     *
     * @param location the location
     * @return the location to string
     */
    public Map<String, Double> getLocationToString(LocationBean location) {
        Map<String, Double> locationMap = new HashMap<>();
        locationMap.put("latitude", location.latitude);
        locationMap.put("longitude", location.longitude);
        return locationMap;
    }
}