package com.opc.gps.bean;


import java.util.Date;
import java.util.UUID;


public class VisitedLocationBean {

    /**
     * The User id.
     */
    public final UUID userId;

    /**
     * The Location.
     */
    public final LocationBean location;

    /**
     * The Time visited.
     */
    public final Date timeVisited;

    /**
     * Instantiates a new Visited location bean.
     *
     * @param userId      the user id
     * @param location    the location
     * @param timeVisited the time visited
     */
    public VisitedLocationBean(UUID userId, LocationBean location, Date timeVisited) {
        this.userId      = userId;
        this.location    = location;
        this.timeVisited = timeVisited;
    }
}