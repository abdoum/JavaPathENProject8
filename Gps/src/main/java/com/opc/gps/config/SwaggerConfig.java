package com.opc.gps.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@EnableSwagger2
@Configuration
public class SwaggerConfig {

    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                                                      .apis(RequestHandlerSelectors.basePackage("com.opc.gps" +
                                                                                                ".controller"))
                                                      .paths(PathSelectors.any())
                                                      .build()
                                                      .useDefaultResponseMessages(false)
                                                      .apiInfo(getApiInfo());

    }

    private ApiInfo getApiInfo() {
        return new ApiInfo(
                "TourGuide's Gps Application",
                "This API represents gps services accessible by the TourGuide application suite",
                "0.0.1",
                "Opensource project",
                new Contact("Abdallah MANSOUR", "http://gitlab.com/abdoum",
                        "abdouom@fakemail.com"),
                "GNU licence",
                "http://gitlab.com/abdoum",
                Collections.emptyList()
        );
    }
}