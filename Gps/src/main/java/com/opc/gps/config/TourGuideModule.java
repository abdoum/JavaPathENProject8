package com.opc.gps.config;

import gpsUtil.GpsUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
public class TourGuideModule {

    /**
     * Creates a gps util instance.
     *
     * @return the gps util instance
     */
    @Bean
    public GpsUtil getGpsUtil() {
        return new GpsUtil();
    }
}