package com.opc.gps.proxy;

import com.opc.gps.bean.UserBean;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@FeignClient(name = "${reward.server.name}", url = "${reward.server.baseUrl}")
public interface IRewardProxy {

    /**
     * Calculate rewards.
     *
     * @param userId the user id
     */
    @GetMapping("/{userId}")
    void calculateRewards(@PathVariable("userId") UUID userId);

    /**
     * Calculate rewards async.
     *
     * @param userBeanList the user bean list
     */
    @PostMapping("/async")
    void calculateRewardsAsync(@RequestBody List<UserBean> userBeanList);

    /**
     * Gets attraction reward points.
     *
     * @param attractionId the attraction id
     * @param userId       the user id
     * @return the attraction reward points
     */
    @GetMapping("/points/{attractionId}/{userId}")
    int getAttractionRewardPoints(@PathVariable("attractionId") UUID attractionId,
                                  @PathVariable("userId") UUID userId);
}