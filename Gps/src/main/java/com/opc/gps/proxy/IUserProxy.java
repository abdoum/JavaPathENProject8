package com.opc.gps.proxy;

import com.opc.gps.bean.UserBean;
import com.opc.gps.bean.UserRewardBean;
import org.hibernate.validator.constraints.Range;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@FeignClient(name = "${user.server.name}", url = "${user.server.baseUrl}")
public interface IUserProxy {

    /**
     * Gets user.
     *
     * @param userId the user id
     * @return the user
     */
    @GetMapping({"/{userId}"})
    UserBean getUser(@PathVariable("userId") UUID userId);

    /**
     * Gets all users.
     *
     * @return the all users
     */
    @GetMapping({"/"})
    Map<String, UserBean> getAllUsers();

    /**
     * Generates a specific number of users.
     *
     * @param usersNumber the number of users to be generated
     * @return a Map of the generated users
     */
    @GetMapping({"/{count}"})
    Map<UUID, UserBean> generateUsers(@PathVariable("count") int usersNumber);

    /**
     * Add user.
     *
     * @param userBean the user bean
     */
    @PostMapping({"/add"})
    void addUser(@RequestBody UserBean userBean);

    /**
     * Gets all users last location.
     *
     * @return the all users last location
     */
    @GetMapping({"/locations"})
    Map<String, Map<String, Double>> getAllUsersLastLocation();

    /**
     * Gets user rewards.
     *
     * @param userId the user id
     * @return the user rewards
     */
    @GetMapping({"/rewards/{userId}"})
    List<UserRewardBean> getUserRewards(@PathVariable("userId") UUID userId);

    /**
     * Sets internal user number.
     *
     * @param usersNumber the users number
     * @return the internal user number
     */
    @PutMapping({"/count/{count}"})
    int setInternalUserNumber(@PathVariable("count") @Range(min = 0, max = 100_000) int usersNumber);
}