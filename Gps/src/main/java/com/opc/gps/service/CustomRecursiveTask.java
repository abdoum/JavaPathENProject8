package com.opc.gps.service;

import com.opc.gps.bean.LocationBean;
import com.opc.gps.bean.UserBean;
import com.opc.gps.bean.VisitedLocationBean;
import gpsUtil.GpsUtil;
import gpsUtil.location.VisitedLocation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;

@Slf4j
public class CustomRecursiveTask extends RecursiveTask<List<VisitedLocationBean>> {

    @Autowired
    private GpsUtil gpsUtil;

    private final List<UserBean> userBeanList;

    private final List<VisitedLocationBean> visitedLocationList = new ArrayList<>();

    private static final int THRESHOLD = 20;

    public CustomRecursiveTask(List<UserBean> userBeanList) {
        this.userBeanList = userBeanList;
    }

    @Override
    protected List<VisitedLocationBean> compute() {
        if (userBeanList.size() > THRESHOLD) {
            return ForkJoinTask.invokeAll(createSubtasks())
                               .stream()
                               .map(customRecursiveTask -> customRecursiveTask.join().get(0))
                               .collect(Collectors.toList());
        }
        else {
            return processing(userBeanList);
        }
    }

    private Collection<CustomRecursiveTask> createSubtasks() {
        List<CustomRecursiveTask> dividedTasks = new ArrayList<>();
        dividedTasks.add(new CustomRecursiveTask(
                userBeanList.subList(0, userBeanList.size() / 2)));
        dividedTasks.add(new CustomRecursiveTask(
                userBeanList.subList(userBeanList.size() / 2, userBeanList.size())));
        return dividedTasks;
    }

    private List<VisitedLocationBean> processing(List<UserBean> userBeanList) {
        userBeanList.forEach(userBean -> {
            try {
                addLocationToHistory(userBean);
            }
            catch (ExecutionException | InterruptedException e) {
                log.error("error when calculating user location", e);
                Thread.currentThread().interrupt();
            }
        });
        return visitedLocationList;
    }

    private UserBean addLocationToHistory(
            UserBean userBean) throws ExecutionException, InterruptedException {
        CompletableFuture<VisitedLocation> completableFuture
                = CompletableFuture.supplyAsync(() -> gpsUtil.getUserLocation(userBean.getUserId()));

        var future = completableFuture
                .thenApplyAsync(userLocation -> {
                    VisitedLocationBean visitedLocation = new VisitedLocationBean(userBean.getUserId(),
                            new LocationBean(userLocation.location.latitude, userLocation.location.longitude)
                            , userLocation.timeVisited);
                    userBean.addToVisitedLocations(visitedLocation);
                    visitedLocationList.add(visitedLocation);
                    return userBean;
                });
        return future.get();
    }

}