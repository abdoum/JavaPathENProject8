package com.opc.gps.service;

import com.opc.gps.bean.LocationBean;
import com.opc.gps.bean.UserBean;
import com.opc.gps.bean.VisitedLocationBean;
import com.opc.gps.exception.EntityNotFoundException;
import com.opc.gps.model.NearByAttraction;
import gpsUtil.location.Attraction;

import java.util.List;
import java.util.UUID;

public interface IGpsService {

    int getAttractionProximityRange();

    int setAttractionProximityRange(int attractionProximityRange);

    VisitedLocationBean getUserLocation(UUID userId) throws EntityNotFoundException;

    boolean isWithinAttractionProximity(LocationBean attraction, LocationBean location);

    int setProximityBuffer(int proximityBuffer);

    VisitedLocationBean trackUserLocation(UUID userId) throws EntityNotFoundException;

    List<VisitedLocationBean> trackUserLocationAsync(List<UserBean> userBeanList);

    int getProximityBuffer();

    List<NearByAttraction> getNearByAttractions(VisitedLocationBean visitedLocation);

    double getDistance(LocationBean loc1, LocationBean loc2);

    void addShutDownHook();

    List<VisitedLocationBean> trackUserLocationAsyncForACount(int usersCount);
}