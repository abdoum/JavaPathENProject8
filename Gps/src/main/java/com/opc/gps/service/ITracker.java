package com.opc.gps.service;

public interface ITracker extends Runnable {

    void stopTracking();

    @Override void run();
}