package com.opc.gps;

import com.opc.gps.bean.UserBean;
import com.opc.gps.service.GpsService;
import gpsUtil.GpsUtil;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(properties = {
        "user.server.baseUrl=http://localhost:9002/users"
})
@ContextConfiguration(initializers = ConfigDataApplicationContextInitializer.class)
class PerformanceIT {

    @Autowired
    GpsService gpsService;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${user.server.baseUrl}")
    String userServerBaseUrl;

    @BeforeEach
    void setUp() {
        Locale.setDefault(Locale.US);
    }

    @Test
    void highVolumeTrackLocation() {
        // Arrange
        int usersNumber = 10_000;
        // Users should be incremented up to 100,000, and test finishes within 15 minutes
        restTemplate.put(userServerBaseUrl + "/count/" + usersNumber, null);
        ResponseEntity<Map<String, UserBean>> allUsers = restTemplate
                .exchange(userServerBaseUrl, HttpMethod.GET, null,
                        new ParameterizedTypeReference<Map<String, UserBean>>() {
                        });

        List<UserBean> userList = new ArrayList<>(allUsers.getBody().values());

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        gpsService.trackUserLocationAsync(userList);

        stopWatch.stop();
        gpsService.addShutDownHook();
        assertTrue(userList.get(0).getVisitedLocations().size() > 3);

        System.out.println(
                "highVolumeTrackLocation: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) +
                " seconds for " + userList.size() + " users.");
        assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
    }
}