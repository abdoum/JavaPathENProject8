package com.opc.gps.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.opc.gps.bean.LocationBean;
import com.opc.gps.bean.VisitedLocationBean;
import com.opc.gps.service.IGpsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class GpsControllerTest {

    @MockBean
    IGpsService iGpsService;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @BeforeEach
    void setUp() {
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    }

    @Test
    void testTrackUserLocation_shouldReturnHttpsStatus200AndAVisitedLocation_forAValidUUID() throws Exception {
        // Arrange
        UUID userId = UUID.randomUUID();
        LocationBean location = new LocationBean(10.0, 10.0);

        VisitedLocationBean visitedLocation = new VisitedLocationBean(userId, location, new Date(1L));
        when(iGpsService.trackUserLocation(any())).thenReturn(visitedLocation);
        // Act and Assert
        mockMvc.perform(post("/gps/track/" + userId)
                       .contentType(MediaType.APPLICATION_JSON)
                       .content(mapper.writeValueAsString(userId)))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.userId", is(userId.toString())));
    }

    @Test
    void testTrackUserLocationAsync_shouldReturnHttpsStatus200AndAVisitedLocation_forAValidUUID() throws Exception {
        // Arrange
        UUID userId = UUID.randomUUID();
        LocationBean location = new LocationBean(10.0, 10.0);

        VisitedLocationBean visitedLocation = new VisitedLocationBean(userId, location, new Date(1L));
        when(iGpsService.trackUserLocationAsync(any())).thenReturn(
                new ArrayList<>(Collections.singleton(visitedLocation)));
        // Act and Assert
        mockMvc.perform(get("/gps/track/async/{usersCount}", 2))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$").isArray());
    }

    @Test
    void testGetNearByAttractions_shouldReturnHttpsStatus200AndAListOfVisitedAttractions_forAValidVisitedLocation() throws
            Exception {
        // Arrange
        UUID userId = UUID.randomUUID();
        LocationBean location = new LocationBean(10.0, 10.0);
        iGpsService.setAttractionProximityRange(100);
        iGpsService.setProximityBuffer(10);
        VisitedLocationBean visitedLocation = new VisitedLocationBean(userId, location, new Date(1L));
        when(iGpsService.getNearByAttractions(any())).thenReturn(new ArrayList<>(5));
        // Act and Assert
        mockMvc.perform(post("/gps/nearByAttractions")
                       .contentType(MediaType.APPLICATION_JSON)
                       .content(mapper.writeValueAsString(visitedLocation)))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$").isArray());
    }

    @Test
    void testStopTracking_shouldReturnHttpsStatus200() throws Exception {
        // Arrange
        doNothing().when(this.iGpsService).addShutDownHook();
        // Act and Assert
        mockMvc.perform(post("/gps/track/stop"))
               .andExpect(status().isOk());
    }

    @Test
    void testGetUserLocation_shouldReturnTheUserLocation_forAValidUserId() throws Exception {
        // Arrange
        UUID userId = UUID.fromString("87c23ee4-6dce-46a2-aadf-e21a19988808");
        VisitedLocationBean visitedLocation = new VisitedLocationBean(userId,
                new LocationBean(10.0, 10.0), new Date(1L));
        when(iGpsService.getUserLocation(any())).thenReturn(visitedLocation);

        // Act and Assert
        mockMvc.perform(get("/gps/locations/" + userId))
               .andExpect(status().isOk());
    }

    @Test
    void testSetAttractionAttractionProximityRange_shouldReturn33_forAProvidedAttractionProximityRangeOf33() throws
            Exception {
        mockMvc.perform(post("/gps/proximityRange")
                       .contentType(MediaType.APPLICATION_JSON)
                       .content("33"))
               .andExpect(status().isOk());
    }

    @Test
    void testGetAttractionRewardPoints_shouldReturn10() throws Exception {
        when(iGpsService.getAttractionProximityRange()).thenReturn(10);
        mockMvc.perform(get("/gps/proximityRange"))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$", is(10)));
    }

    @Test
    void testSetProximityBuffer_shouldReturn44_forAProvidedProximityBufferOf44() throws Exception {
        when(iGpsService.setProximityBuffer(44)).thenReturn(44);
        mockMvc.perform(post("/gps/proximityBuffer")
                       .contentType(MediaType.APPLICATION_JSON)
                       .content("44"))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$", is(44)));
    }

    @Test
    void testGetDistance_shouldReturn0_forTwoIdenticalLocations() throws Exception {
        // Arrange
        LocationBean attraction = new LocationBean(9828D, 98234D);
        LocationBean location = new LocationBean(0987D, 093D);

        Map<String, LocationBean> body = new HashMap<>();
        body.put("attraction", attraction);
        body.put("location", location);

        // Act and Assert
        mockMvc.perform(post("/gps/distance")
                       .contentType(MediaType.APPLICATION_JSON)
                       .content(new ObjectMapper().writeValueAsString(body)))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$", is(0.0)));
    }

    @Test
    void testIsWithinAttractionProximity_shouldReturnA200HttpStatusCodeAndTrue_forALocationWithinAttractionProximity() throws
            Exception {
        LocationBean attraction = new LocationBean(345434.11111, 93343453.3533463);
        LocationBean location = new LocationBean(345434.11111, 93343453.3533463);
        when(iGpsService.isWithinAttractionProximity(any(), any())).thenReturn(
                Boolean.TRUE);
        Map<String, LocationBean> body = new HashMap<>();
        body.put("attraction", attraction);
        body.put("location", location);
        mockMvc.perform(post("/gps/isWithinAttractionProximity")
                       .contentType(MediaType.APPLICATION_JSON)
                       .content(new ObjectMapper().writeValueAsString(body)))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$", is(true)));

    }
}