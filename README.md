![](https://img.shields.io/badge/Java-ED8B00?style=for-the-badge&logo=java&logoColor=white)
![](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
![](/gitlab/last-commit/gitlab-org/gitlab-development-kit)
![](/maven/v/metadata-url/repo1.maven.org/maven2/com/google/code/gson/gson/maven-metadata.xml)
![](/gitlab/coverage/:abdoum/:DA_Java_P8/:develop)

# Tour Guide

![](TourGuide/src/main/resources/static/logo.jpeg)

[[_TOC_]]

## Description

The application is the main entry point of 
the [TourGuide](https://gitlab.com/abdoum/DA_Java_P8/-/tree/develop) application
it acts as a gateway to application running the different services for this main application.

## Requirements

----
- Unix or Windows OS
- [Java](https://www.java.com/), [Apache](https://httpd.apache.org/docs/) and [Gradle](https://gradle.org/) Wrapper
- [Docker engine](https://docs.docker.com/get-started/)

## Installation and Getting Started

----
**To run locally:**

1. Clone this repo to your machine and run using the following commands.

```shell
$ git clone https://gitlab.com/abdoum/DA_Java_P8/-/tree/develop
$ cd DA_Java_P8/
```

2. Make sure you have [docker engine](https://docs.docker.com/get-started/) installed and running on your machine


```shell
$ docker-compose up
```

3. Visit [http://localhost:9001/swagger-ui.html#!/tour45guide45controller](http://localhost:9001/swagger-ui.html#!/tour45guide45controller) **and start experimenting!**

----

## Documentation

Checkout the project [documentation](../wikis/Présentation)

### Before Optimization:

```mermaid
graph TD;
    A[Web client] -->|Http request| B{TourGuide App}
    B --> C(TourGuide Service)
    C -->|Coordinates| D[fa:fa-map GpsUtil.jar]
    C -->|Price| E[fa:fa-dollar-sign TripPricer.jar]
    C -->|Reward| F[fa:fa-trophy RewardCentral.jar]
```

### After Optimization:

```mermaid
graph TD
    A[Web client] -->|Http request| B{fa:fa-box-open TourGuide App}
    B --> |Feign Http Request|C(fa:fa-box-open User App)
    B --> |Feign Http Request|D(fa:fa-box-open Gps App)
    B --> |Feign Http Request|E(fa:fa-box-open Reward App)
    D --> |Coordinates| F[fa:fa-map GpsUtil.jar]
    E -->|Price| G[fa:fa-dollar-sign TripPricer.jar]
    E -->|Reward| H[fa:fa-trophy RewardCentral.jar]

 classDef orange fill:#f967,stroke:#333,stroke-width:4px;
 classDef red fill:#f229,stroke:#333,stroke-width:4px;
 classDef purple fill:#f665,stroke:#333,stroke-width:4px;
     class B orange
     class C,D,E red
     class F,G,H purple
```

---

## Performance improvements



|             | Users          | Time (Seconds)    |   Improved Time (Seconds)   |   Time (Duration hh:mm:ss) | After Optimization (Duration hh:mm:ss) |     
|:---         | :---     |    :----:      |       :---:       |        :---:    |      :---:    | 
|Get Locations|   100    | 7              | 0                 |   0:00:07 |	0:00:00  | 
|             |   1000   | 75             | 9                 |   0:01:15 |	0:00:09  | 
|             |   5 000  | 376            | 41                |   0:06:16 |	0:00:41  | 
|             |   10 000 | 762            | 91                |   0:12:42 |	0:01:31  | 
|             |   50 000 | 3791           | 467               |   1:03:11 |	0:07:47  | 
|             |   100 000| 7579           | 906               |   2:06:19 |	0:15:06  | 

![Rewards graph](TourGuide/src/main/resources/static/Get_Location.svg)

|             | Users    | Time (Seconds) | Improved Time (Seconds)     |     Time (Duration hh:mm:ss) | After Optimization (Duration hh:mm:ss) |  
|:---         | :---     |    :----:      |          :---:     |       :---:     |      :---:     |     
| Get Rewards |   100    | 44             | 5                  |    0:00:44	|  0:00:05   |  
|             |   1000   | 472            | 13                 |    0:07:52	|  0:00:13   |  
|             |   10 000 | 5820           | 86                |     1:37:00	|  0:01:26   |  
|             |   100 000| 64020          | 810               |     17:47:00 |  0:13:30    | 

![Rewards graph](TourGuide/src/main/resources/static/Get_Rewards.svg)

## Architecture
<img src="TourGuide/src/main/resources/static/uml_diagram.png" width="1100">
