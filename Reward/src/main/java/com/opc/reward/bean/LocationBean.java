package com.opc.reward.bean;

public class LocationBean {

    /**
     * The Longitude.
     */
    public final double longitude;

    /**
     * The Latitude.
     */
    public final double latitude;

    /**
     * Instantiates a new Location bean.
     *
     * @param latitude  the latitude
     * @param longitude the longitude
     */
    public LocationBean(double latitude, double longitude) {
        this.latitude  = latitude;
        this.longitude = longitude;
    }
}