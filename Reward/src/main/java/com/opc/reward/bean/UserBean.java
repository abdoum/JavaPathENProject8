package com.opc.reward.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.opc.reward.model.UserPreferences;
import com.opc.reward.model.UserReward;
import gpsUtil.location.Location;
import lombok.Data;
import tripPricer.Provider;

import java.util.*;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserBean {

    private final UUID userId;

    private final String userName;

    private String phoneNumber;

    private String emailAddress;

    private Date latestLocationTimestamp;

    private List<VisitedLocationBean> visitedLocations = new ArrayList<>();

    private List<UserReward> userRewards = new ArrayList<>();

    private UserPreferences userPreferences = new UserPreferences();

    private List<Provider> tripDeals = new ArrayList<>();

    /**
     * Instantiates a new User.
     *
     * @param userId       the user id
     * @param userName     the username
     * @param phoneNumber  the phone number
     * @param emailAddress the email address
     */
    public UserBean(UUID userId, String userName, String phoneNumber, String emailAddress) {
        this.userId       = userId;
        this.userName     = userName;
        this.phoneNumber  = phoneNumber;
        this.emailAddress = emailAddress;
    }

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public UUID getUserId() {
        return userId;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Gets phone number.
     *
     * @return the phone number
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets phone number.
     *
     * @param phoneNumber the phone number
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Gets email address.
     *
     * @return the email address
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets email address.
     *
     * @param emailAddress the email address
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * Gets latest location timestamp.
     *
     * @return the latest location timestamp
     */
    public Date getLatestLocationTimestamp() {
        return latestLocationTimestamp;
    }

    /**
     * Sets latest location timestamp.
     *
     * @param latestLocationTimestamp the latest location timestamp
     */
    public void setLatestLocationTimestamp(Date latestLocationTimestamp) {
        this.latestLocationTimestamp = latestLocationTimestamp;
    }

    /**
     * Add to visited locations.
     *
     * @param visitedLocation the visited location
     */
    public void addToVisitedLocations(VisitedLocationBean visitedLocation) {
        visitedLocations.add(visitedLocation);
    }

    /**
     * Gets visited locations.
     *
     * @return the visited locations
     */
    public List<VisitedLocationBean> getVisitedLocations() {
        return visitedLocations;
    }

    /**
     * Clear visited locations.
     */
    public void clearVisitedLocations() {
        visitedLocations.clear();
    }

    /**
     * Add user reward.
     *
     * @param userReward the user reward
     */
    public void addUserReward(UserReward userReward) {
        if (userRewards.stream()
                       .noneMatch(reward -> reward.attraction.attractionName.equals(
                               userReward.attraction.attractionName))) {
            userRewards.add(userReward);
        }
    }

    /**
     * Gets user rewards.
     *
     * @return the user rewards
     */
    public List<UserReward> getUserRewards() {
        return userRewards;
    }

    /**
     * Gets user preferences.
     *
     * @return the user preferences
     */
    public UserPreferences getUserPreferences() {
        return userPreferences;
    }

    /**
     * Sets user preferences.
     *
     * @param userPreferences the user preferences
     */
    public void setUserPreferences(UserPreferences userPreferences) {
        this.userPreferences = userPreferences;
    }

    /**
     * Gets last visited location.
     *
     * @return the last visited location
     */
    public VisitedLocationBean getLastVisitedLocation() {
        return visitedLocations.get(visitedLocations.size() - 1);
    }

    /**
     * Gets trip deals.
     *
     * @return the trip deals
     */
    public List<Provider> getTripDeals() {
        return tripDeals;
    }

    /**
     * Sets trip deals.
     *
     * @param tripDeals the trip deals
     */
    public void setTripDeals(List<Provider> tripDeals) {
        this.tripDeals = tripDeals;
    }

    /**
     * Gets location to string.
     *
     * @param location the location
     * @return the location to string
     */
    public Map<String, Double> getLocationToString(Location location) {
        Map<String, Double> locationMap = new HashMap<>();
        locationMap.put("latitude", location.latitude);
        locationMap.put("longitude", location.longitude);
        return locationMap;
    }
}