package com.opc.reward.config;

import gpsUtil.GpsUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import rewardCentral.RewardCentral;

@Configuration
public class TourGuideModule {

    /**
     * Creates a gps util instance.
     *
     * @return the gps util instance
     */
    @Bean
    public GpsUtil getGpsUtil() {
        return new GpsUtil();
    }

    /**
     * Creates a reward central instance.
     *
     * @return the reward central instance
     */
    @Bean
    public RewardCentral getRewardCentral() {
        return new RewardCentral();
    }

}