package com.opc.reward.controller;

import com.opc.reward.bean.UserBean;
import com.opc.reward.exception.EntityNotFoundException;
import com.opc.reward.service.IRewardService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tripPricer.Provider;

import java.util.List;
import java.util.UUID;

@Validated
@RestController
@Slf4j
@RequestMapping("/rewards")
public class RewardController {

    @Autowired
    IRewardService iRewardService;

    /**
     * Calculate rewards for a list of users. This initiates the reward calculation. No result is return form this
     * process.
     *
     * @param userBeanList the user id for whom the reward calculation must be performed
     */
    @ApiOperation(
            value = "Calculate rewards for a list of users. This initiates the reward calculation. No result is " +
                    "return form " +
                    "this " +
                    "process.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "empty response"),
            }
    )
    @PostMapping("/async")
    public void calculateRewardsAsync(@RequestBody List<UserBean> userBeanList) {
        log.info("Received calculate rewards async request.");
        iRewardService.calculateRewardsAsync(userBeanList);
    }

    /**
     * Calculate rewards. This initiates the reward calculation. No result is return form this process.
     *
     * @param userId the user id for whom the reward calculation must be performed
     * @throws EntityNotFoundException if no user corresponding to the provided userId is found
     */
    @ApiOperation(
            value = "Calculate rewards. This initiates the reward calculation. No result is return form this process.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "empty response"),
                    @ApiResponse(code = 404, message = "In case the user is not found with the provided user id.")
            }
    )
    @GetMapping("/{userId}")
    public void calculateRewards(@PathVariable("userId") UUID userId) throws EntityNotFoundException {
        log.info("Received calculate rewards request.");
        iRewardService.calculateRewardsById(userId);
    }

    /**
     * Gets the attraction reward points.
     *
     * @param attractionId the attractio id
     * @param userId       the user id
     * @return the attraction reward points
     * @throws EntityNotFoundException the entity not found exception
     */
    @ApiOperation("Gets the attraction reward points.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "The attraction reward points"),
            }
    )
    @GetMapping("/points/{attractionId}/{userId}")
    public int getAttractionRewardPoints(@PathVariable("attractionId") UUID attractionId,
                                         @PathVariable("userId") UUID userId) throws EntityNotFoundException {
        return iRewardService.getAttractionRewardPoints(attractionId, userId);
    }

    /**
     * Get providers for a specific use
     *
     * @param userId UUID the user id
     * @return a list of providers
     * @throws EntityNotFoundException if user is not found
     */
    @ApiOperation(value = "Get providers for a specific user")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "The list of providers"),
                    @ApiResponse(code = 400, message = "In case the user is not found with the provided user id.")
            }
    )
    @GetMapping("/providers/{userId}")
    public List<Provider> getProviders(@PathVariable("userId") UUID userId) throws EntityNotFoundException {
        return iRewardService.getProviders(userId);
    }

}