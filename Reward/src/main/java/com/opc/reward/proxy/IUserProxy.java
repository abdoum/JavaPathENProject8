package com.opc.reward.proxy;

import com.opc.reward.bean.UserBean;
import com.opc.reward.exception.EntityNotFoundException;
import com.opc.reward.model.UserReward;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;


@FeignClient(name = "${user.server.name}", url = "${user.server.baseUrl}")
public interface IUserProxy {

    /**
     * Gets a user by its id.
     *
     * @param userId the user id
     * @return User the found user
     * @throws EntityNotFoundException the entity not found exception
     */
    @GetMapping({"/{userId}"})
    UserBean getUser(@PathVariable("userId") UUID userId) throws EntityNotFoundException;

    /**
     * Gets all users.
     *
     * @return the all users
     */
    @GetMapping({"/"})
    Map<String, UserBean> getAllUsers();

    /**
     * Sets the internal user number to be generated.
     *
     * @param usersNumber the number of users
     * @return the new number of users that has been set.
     */
    @PutMapping({"/count/{count}"})
    int setInternalUserNumber(@PathVariable("count") int usersNumber);

    /**
     * Adds a user.
     *
     * @param user the added user
     */
    @PostMapping({"/"})
    void addUser(@RequestBody UserBean user);

    /**
     * Gets user rewards.
     *
     * @param userId the user id
     * @return the user rewards
     */
    @GetMapping("/rewards/{userId}")
    List<UserReward> getUserRewards(@PathVariable("userId") UUID userId);
}