package com.opc.reward.service;

import com.opc.reward.bean.LocationBean;
import com.opc.reward.bean.UserBean;
import com.opc.reward.bean.VisitedLocationBean;
import com.opc.reward.exception.EntityNotFoundException;
import com.opc.reward.model.UserReward;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import tripPricer.Provider;

import java.util.List;
import java.util.UUID;

public interface IRewardService {

    /**
     * Gets providers.
     *
     * @param userId the user id
     * @return A list of providers
     * @throws EntityNotFoundException if the user is not found
     */
    List<Provider> getProviders(UUID userId) throws EntityNotFoundException;

    /**
     * Calculates rewards for a user.
     *
     * @param userBean the user
     */
    void calculateRewards(UserBean userBean);

    /**
     * Calculates rewards asynchronously for multiple users.
     *
     * @param userBeanList the users list
     */
    void calculateRewardsAsync(List<UserBean> userBeanList);

    /**
     * Checks if a user has no rewards for a given attraction.
     * This method uses the attraction name as a comparator.
     *
     * @param user       the user
     * @param attraction the attraction to verify against
     * @return a boolean, true if the user has no rewards for the given attraction, false otherwise.
     */
    default boolean isUserHasNoRewards(UserBean user, Attraction attraction) {
        return user.getUserRewards().stream().noneMatch(reward ->
                reward.attraction.attractionName.equals(attraction.attractionName)
        );
    }

    /**
     * Creates a user reward.
     *
     * @param user            the user
     * @param visitedLocation the visitedLocation
     * @param attraction      the attraction
     * @return the created user reward
     */
    default UserReward createUserReward(UserBean user, VisitedLocationBean visitedLocation,
                                        Attraction attraction) throws EntityNotFoundException {
        int rewardPoints = this.getAttractionRewardPoints(attraction.attractionId, user.getUserId());
        return new UserReward(visitedLocation, attraction, rewardPoints);
    }

    /**
     * Gets an attraction's reward points.
     *
     * @param attractionId the attraction id
     * @param userId       the user id
     * @return the attraction's reward points
     * @throws EntityNotFoundException the entity not found exception if no user is found with the provided id
     */
    int getAttractionRewardPoints(UUID attractionId, UUID userId) throws EntityNotFoundException;

    /**
     * Checks if an attraction is within proximity range.
     *
     * @param attraction the attraction
     * @param location   the location
     * @return a boolean, true if the attraction is within proximity range, false otherwise
     */
    boolean isWithinAttractionProximity(Attraction attraction, LocationBean location);

    /**
     * Gets the distance between 2 locations.
     *
     * @param loc1 the first location
     * @param loc2 the second location
     * @return the distance
     */
    double getDistance(Location loc1, LocationBean loc2);

    void calculateRewardsById(UUID userId) throws EntityNotFoundException;
}