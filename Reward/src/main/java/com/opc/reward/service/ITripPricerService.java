package com.opc.reward.service;

import com.opc.reward.bean.UserBean;
import tripPricer.Provider;

import java.util.List;

public interface ITripPricerService {

    List<Provider> getProviders(UserBean userBean);
}