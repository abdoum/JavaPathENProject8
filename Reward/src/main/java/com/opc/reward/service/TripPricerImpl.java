package com.opc.reward.service;

import tripPricer.Provider;
import tripPricer.TripPricer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import static org.reflections.Reflections.log;

/**
 * This class is an implementation of Trip pricer getPrice method to fix some calculation errors since the original
 * class is compiled and cannot be modified. This implementation will be used instead of the original class.
 */
public class TripPricerImpl extends TripPricer {

    /**
     * This gets a list of providers for an attraction using the user's preferences parameters
     *
     * @param apiKey        the api key
     * @param attractionId  the attraction uuid
     * @param adults        adults count
     * @param children      children count
     * @param nightsStay    nightsStay count
     * @param rewardsPoints rewardsPoints count
     * @return a list of Providers matching the given parameters
     */
    @Override
    public List<Provider> getPrice(String apiKey, UUID attractionId, int adults, int children, int nightsStay,
                                   int rewardsPoints) {
        List<Provider> providers = new ArrayList<>();
        HashSet<String> providersUsed = new HashSet<>();

        try {
            TimeUnit.MILLISECONDS.sleep(ThreadLocalRandom.current().nextInt(1, 50));
        }
        catch (InterruptedException e) {
            log.error("error when calculating reward", e);
            Thread.currentThread().interrupt();
        }

        for (int i = 0; i < 5; ++i) {
            int multiple = ThreadLocalRandom.current().nextInt(100, 700);
            double childrenDiscount = ((double) children / 3);
            double price =
                    (((double) multiple * adults) + (multiple * childrenDiscount)) *
                    ((nightsStay + 0.99D) - (rewardsPoints));
            if (price < 0D) {
                price = 0D;
            }

            String provider = "";

            do {
                provider = this.getProviderName(apiKey, adults);
            } while (providersUsed.contains(provider));

            providersUsed.add(provider);
            providers.add(new Provider(attractionId, provider, price));
        }

        return providers;
    }
}