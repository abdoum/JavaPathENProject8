package com.opc.reward.service;

import com.opc.reward.bean.UserBean;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(properties = {
        "user.server.baseUrl=http://localhost:9002/users", "gps.server.baseUrl=http://localhost" +
                                                           ":9007/gps"
})
@ContextConfiguration(initializers = ConfigDataApplicationContextInitializer.class)
@ExtendWith(SpringExtension.class)
class PerformanceIT {

    @Autowired
    IRewardService iRewardService;

    @Value("${gps.server.baseUrl}")
    String gpsServerBasUrl;

    @Value("${user.server.baseUrl}")
    String userServerBaseUrl;

    @Autowired
    private RestTemplate restTemplate;

    @Test
    void testHighVolumeGetRewards_shouldFinishInLessThan20Minutes_for100_000users() {
        // Arrange
        int usersNumber = 100000;
        //  Set users count to generate
        restTemplate.put(userServerBaseUrl + "/count/" + usersNumber, null);
        ResponseEntity<Map<String, UserBean>> allUsers = restTemplate
                .exchange(userServerBaseUrl, HttpMethod.GET, null,
                        new ParameterizedTypeReference<Map<String, UserBean>>() {
                        });

        List<UserBean> userList = new ArrayList<>(allUsers.getBody().values());
        StopWatch stopWatch = new StopWatch();

        // Act and Assert
        stopWatch.start();
        iRewardService.calculateRewardsAsync(userList);
        assertTrue(userList.get(0).getUserRewards().size() > 0);
        stopWatch.stop();
        restTemplate.postForEntity(gpsServerBasUrl + "/track/stop", null, null);
        System.out.println(
                "highVolumeGetRewards: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) +
                " seconds for " + userList.size() + "  users");

        assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
    }
}