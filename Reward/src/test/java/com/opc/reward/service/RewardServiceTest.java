package com.opc.reward.service;

import com.opc.reward.bean.LocationBean;
import com.opc.reward.bean.UserBean;
import com.opc.reward.bean.VisitedLocationBean;
import com.opc.reward.exception.EntityNotFoundException;
import com.opc.reward.proxy.IUserProxy;
import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import rewardCentral.RewardCentral;
import tripPricer.Provider;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {RewardService.class, TripPricerService.class, GpsUtil.class, RewardCentral.class})
@ExtendWith(SpringExtension.class)
class RewardServiceTest {

    @MockBean
    IUserProxy IUserProxy;

    @MockBean
    GpsUtil gpsUtil;

    @MockBean
    ITripPricerService iTripPricerService;

    @Autowired
    private IRewardService iRewardService;

    @Test
    void testCalculateRewards_shouldReturnVoid_forAValidUser() throws EntityNotFoundException {
        // Arrange
        UserBean user = new UserBean(UUID.randomUUID(), "Tom Cruise", "001943876782", "usertom@tourGuide.com");
        VisitedLocationBean locationBean = new VisitedLocationBean(
                UUID.fromString("48346ebe-fb96-4dd7-9ece-b3a6c6261e36"),
                new LocationBean(39.54, 175.03), new Date());
        user.addToVisitedLocations(locationBean);
        when(IUserProxy.getUser(user.getUserId())).thenReturn(user);
        // Act and Assert
        iRewardService.calculateRewards(user);
        assertNotNull(user.getUserRewards());
    }

    @Test
    void testCalculateRewardsAsync_shouldAddRewardsForTheUser_forAValidUser() throws EntityNotFoundException {
        // Arrange
        UserBean user = new UserBean(UUID.randomUUID(), "Tom Cruise", "001943876782", "usertom@tourGuide.com");
        VisitedLocationBean locationBean = new VisitedLocationBean(
                UUID.fromString("48346ebe-fb96-4dd7-9ece-b3a6c6261e36"),
                new LocationBean(39.54, 175.03), new Date());
        user.addToVisitedLocations(locationBean);
        when(IUserProxy.getUser(user.getUserId())).thenReturn(user);
        // Act and Assert
        iRewardService.calculateRewardsAsync(Collections.singletonList(user));
        assertNotNull(user.getUserRewards());
    }

    @DisplayName("should return the attractions points for a valid attraction")
    @Test
    void testGetAttractionRewardPoints_shouldReturnAttractionPoints_forAValidAttraction() throws
            EntityNotFoundException {
        // Arrange
        Location location = new Location(12.343543, 34.234235);
        Attraction attraction = new Attraction("my new attraction", "Paris", "Denver", location.latitude,
                location.longitude);
        UserBean user = new UserBean(UUID.randomUUID(), "Tom Cruise", "001943876782", "usertom@tourGuide.com");
        when(IUserProxy.getUser((UUID) any())).thenReturn(user);
        // Act and Assert
        int rewardPoints = this.iRewardService.getAttractionRewardPoints(attraction.attractionId, user.getUserId());
        assertTrue(rewardPoints > 0);
    }

    @DisplayName("should return the attractions points for a valid attraction")
    @Test
    void testGetAttractionRewardPoints_shouldThrowAnEntityNotFoundException_forANonExistingUser() throws
            EntityNotFoundException {
        // Arrange
        Location location = new Location(12.343543, 34.234235);
        Attraction attraction = new Attraction("my new attraction", "Paris", "Denver", location.latitude,
                location.longitude);
        UUID userId = UUID.randomUUID();
        when(IUserProxy.getUser((UUID) any())).thenReturn(null);
        // Act and Assert
        Exception exception = assertThrows(EntityNotFoundException.class,
                () -> iRewardService.getAttractionRewardPoints(attraction.attractionId, userId
                ));
        assertEquals("no user with id : " + userId + " could be found", exception.getMessage());
    }

    @Test
    void testGetDistance_shouldReturnTheDistanceBetweenTwoLocations_forTwoValidLocations() {
        Location location = new Location(12.343543, 34.234235);
        LocationBean locationBean2 = new LocationBean(12.343543, 34.234235);

        assertEquals(0, iRewardService.getDistance(location, locationBean2), 0);
    }

    @Test
    void testIsWithinAttractionProximity_shouldReturnTrue_forTwoAttractionWithinDefinedTheProximityRange() {
        // Arrange
        LocationBean locationBean2 = new LocationBean(12.343543, 34.234235);
        Attraction attraction = new Attraction("91S9Z", "sample", "street", 12.343543, 34.234235);
        // Act and Assert
        assertTrue(iRewardService.isWithinAttractionProximity(attraction, locationBean2));
    }

    @Test
    void testGetProviders_shouldReturnAListOfProviders_forAValidUserBean() throws EntityNotFoundException {
        // Arrange
        UserBean user = new UserBean(UUID.randomUUID(), "Tom Cruise", "001943876782", "usertom@tourGuide.com");
        when(IUserProxy.getUser((UUID) any())).thenReturn(user);
        List<Provider> providers = Collections.singletonList(new Provider(UUID.randomUUID(), "oppose", 17.90));
        when(iTripPricerService.getProviders((UserBean) any())).thenReturn(providers);
        // Act and Assert
        assertEquals(providers, iRewardService.getProviders(user.getUserId()));
    }

    @Test
    void testCalculateRewardsById_shouldReturnVoid_forAValidUserId() throws EntityNotFoundException {
        // Arrange
        UserBean user = new UserBean(UUID.randomUUID(), "Tom Cruise", "001943876782", "usertom@tourGuide.com");
        VisitedLocationBean locationBean = new VisitedLocationBean(
                UUID.fromString("48346ebe-fb96-4dd7-9ece-b3a6c6261e36"),
                new LocationBean(39.54, 175.03), new Date());

        user.addToVisitedLocations(locationBean);
        when(IUserProxy.getUser(user.getUserId())).thenReturn(user);
        // Act and Assert
        iRewardService.calculateRewardsById(user.getUserId());
        assertNotNull(user.getUserRewards());
        verify(IUserProxy).getUser(user.getUserId());
    }
}