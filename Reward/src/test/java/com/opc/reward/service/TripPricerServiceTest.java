package com.opc.reward.service;

import com.opc.reward.bean.UserBean;
import org.junit.jupiter.api.Test;
import tripPricer.Provider;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;

class TripPricerServiceTest {

    @Test
    void getProviders() {
        ITripPricerService iTripPricerService = new TripPricerService();
        UserBean userBean = new UserBean(UUID.fromString("d78094ae-8b5e-4506-a4c4-8ba0d5360278"), "TomCruise",
                "00178344478", "tom@tourGuide.com");
        List<Provider> providerList = iTripPricerService.getProviders(userBean);
        assertFalse(providerList.isEmpty());
    }
}