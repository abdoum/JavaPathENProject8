package com.opc.tourguide.bean;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.*;

public class UserBean {

    private final UUID userId;

    @NotBlank
    @Size(min= 4, max=50)
    private final String userName;

    @NotBlank
    @Size(min= 6, max=10)
    private String phoneNumber;

    @NotBlank
    @Email
    private String emailAddress;

    @DateTimeFormat
    private Date latestLocationTimestamp;

    private final List<VisitedLocationBean> visitedLocations = new ArrayList<>();

    private final List<UserRewardBean> userRewards = new ArrayList<>();

    private UserPreferencesBean userPreferences = new UserPreferencesBean();

    private List<ProviderBean> tripDeals = new ArrayList<>();

    public UserBean(UUID userId, String userName, String phoneNumber, String emailAddress) {
        this.userId       = userId;
        this.userName     = userName;
        this.phoneNumber  = phoneNumber;
        this.emailAddress = emailAddress;
    }

    public UUID getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setLatestLocationTimestamp(Date latestLocationTimestamp) {
        this.latestLocationTimestamp = latestLocationTimestamp;
    }

    public Date getLatestLocationTimestamp() {
        return latestLocationTimestamp;
    }

    public void addToVisitedLocations(VisitedLocationBean visitedLocation) {
        visitedLocations.add(visitedLocation);
    }

    public List<VisitedLocationBean> getVisitedLocations() {
        return visitedLocations;
    }

    public void clearVisitedLocations() {
        visitedLocations.clear();
    }

    public void addUserReward(UserRewardBean userRewardBean) {
        if (userRewards.stream()
                       .noneMatch(reward -> reward.attraction.attractionName.equals(
                                   userRewardBean.attraction.attractionName))) {
            userRewards.add(userRewardBean);
        }
    }

    public List<UserRewardBean> getUserRewards() {
        return userRewards;
    }

    public UserPreferencesBean getUserPreferences() {
        return userPreferences;
    }

    public void setUserPreferences(UserPreferencesBean userPreferencesBean) {
        this.userPreferences = userPreferencesBean;
    }

    public VisitedLocationBean getLastVisitedLocation() {
        return visitedLocations.get(visitedLocations.size() - 1);
    }

    public LocationBean getLastVisitedLocationCoordinates() {
        return visitedLocations.get(visitedLocations.size() - 1).location;
    }

    public void setTripDeals(List<ProviderBean> tripDeals) {
        this.tripDeals = tripDeals;
    }

    public List<ProviderBean> getTripDeals() {
        return tripDeals;
    }

    public Map<String, Double> getLocationToString(LocationBean location) {
        Map<String, Double> locationMap = new HashMap<>();
        locationMap.put("latitude", location.latitude);
        locationMap.put("longitude", location.longitude);
        return locationMap;
    }
}