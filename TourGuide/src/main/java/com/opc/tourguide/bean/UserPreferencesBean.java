package com.opc.tourguide.bean;

import javax.money.CurrencyUnit;
import javax.money.Monetary;

public class UserPreferencesBean {

	private int attractionProximity = Integer.MAX_VALUE;

	private final CurrencyUnit currency = Monetary.getCurrency("USD");

	private MoneyBean lowerPricePoint = MoneyBean.of(0, currency);

	private MoneyBean highPricePoint = MoneyBean.of(Integer.MAX_VALUE, currency);

	private int tripDuration = 1;

	private int ticketQuantity = 1;

	private int numberOfAdults = 1;

	private int numberOfChildren = 0;

	public UserPreferencesBean() {
		// keep this empty constructor for jackson deserialization
	}

	public void setAttractionProximity(int attractionProximity) {
		this.attractionProximity = attractionProximity;
	}

	public int getAttractionProximity() {
		return attractionProximity;
	}

	public MoneyBean getLowerPricePoint() {
		return lowerPricePoint;
	}

	public void setLowerPricePoint(MoneyBean lowerPricePoint) {
		this.lowerPricePoint = lowerPricePoint;
	}

	public MoneyBean getHighPricePoint() {
		return highPricePoint;
	}

	public void setHighPricePoint(MoneyBean highPricePoint) {
		this.highPricePoint = highPricePoint;
	}

	public int getTripDuration() {
		return tripDuration;
	}


	public void setTripDuration(int tripDuration) {
		this.tripDuration = tripDuration;
	}

	public int getTicketQuantity() {
		return ticketQuantity;
	}

	public void setTicketQuantity(int ticketQuantity) {
		this.ticketQuantity = ticketQuantity;
	}

	public int getNumberOfAdults() {
		return numberOfAdults;
	}

	public void setNumberOfAdults(int numberOfAdults) {
		this.numberOfAdults = numberOfAdults;
	}

	public int getNumberOfChildren() {
		return numberOfChildren;
	}

	public void setNumberOfChildren(int numberOfChildren) {
		this.numberOfChildren = numberOfChildren;
	}

	@Override public String toString() {
		return "UserPreferences{" +
			   "attractionProximity=" + attractionProximity +
			   ", currency=" + currency +
			   ", lowerPricePoint=" + lowerPricePoint +
			   ", highPricePoint=" + highPricePoint +
			   ", tripDuration=" + tripDuration +
			   ", ticketQuantity=" + ticketQuantity +
			   ", numberOfAdults=" + numberOfAdults +
			   ", numberOfChildren=" + numberOfChildren +
			   '}';
	}

}