package com.opc.tourguide.bean;

public class UserRewardBean {
    public final VisitedLocationBean visitedLocation;
    public final AttractionBean attraction;
    private int rewardPoints;

    public UserRewardBean() {
        visitedLocation = null;
        attraction      = null;
    }

    public UserRewardBean(VisitedLocationBean visitedLocation, AttractionBean attraction, int rewardPoints) {
        this.visitedLocation = visitedLocation;
        this.attraction      = attraction;
        this.rewardPoints    = rewardPoints;
    }

    public UserRewardBean(VisitedLocationBean visitedLocation, AttractionBean attraction) {
        this.visitedLocation = visitedLocation;
        this.attraction      = attraction;
    }

    public void setRewardPoints(int rewardPoints) {
        this.rewardPoints = rewardPoints;
    }

    public int getRewardPoints() {
        return this.rewardPoints;
    }
}