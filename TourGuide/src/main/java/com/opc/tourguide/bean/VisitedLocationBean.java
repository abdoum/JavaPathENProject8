package com.opc.tourguide.bean;

import org.springframework.format.annotation.DateTimeFormat;
import com.opc.tourguide.validator.Uuid;

import java.util.Date;
import java.util.UUID;

public class VisitedLocationBean {

    @Uuid
    public final UUID userId;

    public final LocationBean location;

    @DateTimeFormat
    public final Date timeVisited;

    /**
     * Instantiates a new Visited location bean.
     *
     * @param userId      the user id
     * @param location    the location
     * @param timeVisited the time visited
     */
    public VisitedLocationBean(UUID userId, LocationBean location, Date timeVisited) {
        this.userId      = userId;
        this.location    = location;
        this.timeVisited = timeVisited;
    }
}