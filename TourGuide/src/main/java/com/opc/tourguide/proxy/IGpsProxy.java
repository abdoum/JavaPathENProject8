package com.opc.tourguide.proxy;

import com.opc.tourguide.bean.NearByAttractionBean;
import com.opc.tourguide.bean.VisitedLocationBean;
import com.opc.tourguide.exception.EntityNotFoundException;
import org.hibernate.validator.constraints.Range;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Positive;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@FeignClient(name = "${gps.server.name}", url = "${gps.server.baseUrl}")
public interface IGpsProxy {

    /**
     * Gets nearby attractions.
     *
     * @param visitedLocation the visited location
     * @return the near by attractions
     */
    @PostMapping("/nearByAttractions")
    List<NearByAttractionBean> getNearByAttractions(@RequestBody VisitedLocationBean visitedLocation);

    /**
     * Track user location visited location bean.
     *
     * @param userId the user id
     * @return the visited location bean
     * @throws EntityNotFoundException the entity not found exception
     */
    @PostMapping("/track")
    VisitedLocationBean trackUserLocation(@RequestBody UUID userId) throws
            EntityNotFoundException;

    /**
     * Track user location async visited location bean.
     *
     * @param usersCount the users count
     * @return the visited location bean
     * @throws EntityNotFoundException the entity not found exception
     */
    @GetMapping("/track/async/{usersCount}")
    List<VisitedLocationBean> trackUserLocationAsync(
            @PathVariable("usersCount") @Range(min = 1, max = 100_000) int usersCount) throws
            EntityNotFoundException;

    /**
     * Stop tracking.
     */
    @PostMapping("/track/stop")
    void stopTracking();

    /**
     * Gets user location.
     *
     * @param userId the user id
     * @return the user location
     * @throws EntityNotFoundException the entity not found exception
     */
    @GetMapping("/locations/{userId}")
    VisitedLocationBean getUserLocation(@PathVariable("userId") UUID userId) throws
            EntityNotFoundException;

    /**
     * Is within attraction proximity boolean.
     *
     * @param body the body
     * @return the boolean
     */
    @PostMapping("/isWithinAttractionProximity")
    boolean isWithinAttractionProximity(@RequestBody Map<String, Object> body);

    /**
     * Sets proximity buffer.
     *
     * @param buffer the buffer
     * @return the proximity buffer
     */
    @PostMapping("/proximityBuffer")
    int setProximityBuffer(@RequestBody @Positive int buffer);

    /**
     * Sets attraction proximity range.
     *
     * @param range the range
     * @return the attraction proximity range
     */
    @PostMapping("/proximityRange")
    int setAttractionProximityRange(@RequestBody @Positive int range);

    /**
     * Gets attraction proximity range.
     *
     * @return the attraction proximity range
     */
    @GetMapping("/proximityRange")
    int getAttractionProximityRange();

}