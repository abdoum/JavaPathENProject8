package com.opc.tourguide.proxy;

import com.opc.tourguide.bean.ProviderBean;
import com.opc.tourguide.exception.EntityNotFoundException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;
import java.util.UUID;


@FeignClient(name = "${reward.server.name}", url = "${reward.server.baseUrl}")
public interface IRewardProxy {

    /**
     * Calculate rewards.
     *
     * @param userName the user name
     */
    @GetMapping("/{userName}")
    void calculateRewards(@PathVariable("userName") String userName);

    /**
     * Gets attraction proximity range.
     *
     * @return the attraction proximity range
     */
    @GetMapping("/attractionProximityRange")
    int getAttractionProximityRange();

    /**
     * Gets distance.
     *
     * @param body the body
     * @return the distance
     */
    @PostMapping("/distance")
    double getDistance(@RequestBody Map<String, Object> body);

    /**
     * Gets attraction reward points.
     *
     * @param attractionId the attraction id
     * @param userId       the user id
     * @return the attraction reward points
     */
    @GetMapping("/points/{attractionId}/{userId}")
    int getAttractionRewardPoints(@PathVariable("attractionId") UUID attractionId,
                                  @PathVariable("userId") UUID userId);

    /**
     * Gets providers.
     *
     * @param userId the user id
     * @return the providers
     * @throws EntityNotFoundException the entity not found exception
     */
    @GetMapping("/providers/{userId}")
    List<ProviderBean> getProviders(@PathVariable("userId") UUID userId) throws EntityNotFoundException;
}