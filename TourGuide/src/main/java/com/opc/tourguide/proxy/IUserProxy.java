package com.opc.tourguide.proxy;

import com.opc.tourguide.bean.UserBean;
import com.opc.tourguide.bean.UserRewardBean;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;
import java.util.UUID;


@FeignClient(name = "${user.server.name}", url = "${user.server.baseUrl}")
public interface IUserProxy {

    /**
     * Gets user.
     *
     * @param userId the user id
     * @return the user
     */
    @GetMapping({"/{userId}"})
    UserBean getUser(@PathVariable("userId") UUID userId);

    /**
     * Gets all users.
     *
     * @return the all users
     */
    @GetMapping({"/"})
    Map<String, UserBean> getAllUsers();

    /**
     * Add user.
     *
     * @param userBean the user bean
     */
    @PostMapping({"/add"})
    void addUser(@RequestBody UserBean userBean);

    /**
     * Gets all users last location.
     *
     * @return the all users last location
     */
    @GetMapping({"/locations"})
    Map<String, Map<String, Double>> getAllUsersLastLocation();

    /**
     * Gets user rewards.
     *
     * @param userId the user id
     * @return the user rewards
     */
    @GetMapping({"/rewards/{userId}"})
    List<UserRewardBean> getUserRewards(@PathVariable("userId") UUID userId);
}