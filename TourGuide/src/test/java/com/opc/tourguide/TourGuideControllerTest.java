package com.opc.tourguide;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.opc.tourguide.bean.*;
import com.opc.tourguide.proxy.IGpsProxy;
import com.opc.tourguide.proxy.IRewardProxy;
import com.opc.tourguide.proxy.IUserProxy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class TourGuideControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private IUserProxy IUserProxy;

    @MockBean
    private IGpsProxy IGpsProxy;

    @MockBean
    private IRewardProxy IRewardProxy;

    @BeforeEach
    void setUp() {
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    }

    @Test
    void testGetNearByLocation_shouldReturnHttpStatus200AndAListOfNearbyLocations() throws Exception {
        UUID userId = UUID.fromString("5392d8ee-2390-4b81-82ee-9e0d44c0ca5d");

        UserBean user = new UserBean(userId, "weekend", "89263478293", "nifr@kjfbrn.fr");
        when(IUserProxy.getUser(userId)).thenReturn(user);
        VisitedLocationBean visitedLocationBean = new VisitedLocationBean(UUID.randomUUID(), new LocationBean(10.0,
                10.0), new Date(1L));

        List<NearByAttractionBean> nearByAttractionBeans = new ArrayList<NearByAttractionBean>(
                Collections.singleton(new NearByAttractionBean()));
        when(IGpsProxy.getUserLocation((UUID) any())).thenReturn(visitedLocationBean);
        when(IGpsProxy.getNearByAttractions((VisitedLocationBean) any())).thenReturn(nearByAttractionBeans);

        mockMvc.perform(get("/getNearbyAttractions")
                       .param("userId", "ae3a3bb2-7e6f-4295-b165-d67097d22dc3"))
               .andExpect(status().is(200))
               .andExpect(jsonPath("$").isArray())
               .andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    void testGetIndex_shouldReturnHttpStatus200AndAWelcomeMessage() throws Exception {

        // Act and Assert
        mockMvc.perform(get("/"))
               .andExpect(status().is(200))
               .andExpect(content().string("Greetings from TourGuide!"));
    }

    @Test
    void testGetLocation_shouldReturnHttpStatus200AndALocation() throws Exception {
        // Arrange
        VisitedLocationBean locationBean = new VisitedLocationBean(UUID.randomUUID(), new LocationBean(10.0, 10.0)
                , new Date(1L));
        when(IGpsProxy.getUserLocation((UUID) any())).thenReturn(locationBean);

        // Act and Assert
        mockMvc.perform(get("/getLocation")
                       .param("userId", String.valueOf(UUID.randomUUID())))
               .andExpect(status().is(200))
               .andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    void testGetRewards_shouldReturnHttpStatus200AndAListOfRewards() throws Exception {
        // Arrange
        VisitedLocationBean locationBean = new VisitedLocationBean(UUID.randomUUID(), new LocationBean(10.0, 10.0)
                , new Date(1L));
        when(IGpsProxy.getUserLocation((UUID) any())).thenReturn(locationBean);

        // Act and Assert
        mockMvc.perform(get("/getRewards")
                       .param("userId", String.valueOf(UUID.randomUUID())))
               .andExpect(status().is(200))
               .andExpect(jsonPath("$").isArray());
    }

    @Test
    void testGetAllCurrentLocations_shouldReturnHttpStatus200AndAMapOfLocationPerUser() throws Exception {
        // Arrange
        Map<String, Map<String, Double>> usersAndRecentLocations = new HashMap<>();
        Map<String, Double> recentLocations = new HashMap<>();
        recentLocations.put("04a5310d-2dbc-48c6-bd60-8a9a5673d935", 10.0);
        usersAndRecentLocations.put("0e15fc2a-f49e-4265-8e92-420feb77562e", recentLocations);
        when(IUserProxy.getAllUsersLastLocation()).thenReturn(usersAndRecentLocations);
        // Act and Assert
        mockMvc.perform(get("/getAllCurrentLocations"))
               .andExpect(status().is(200))
               .andExpect(jsonPath("$").isMap());
    }

    @Test
    void testGetTripDeals_shouldReturnHttpStatus200AndAListOfProviders() throws Exception {
        // Arrange
        List<ProviderBean> providers = Collections.singletonList(new ProviderBean(UUID.randomUUID(), "quart", 89.0));
        when(IRewardProxy.getProviders((UUID) any())).thenReturn((providers));
        // Act and Assert
        mockMvc.perform(get("/getTripDeals")
                       .param("userId", String.valueOf(UUID.randomUUID())))
               .andExpect(status().is(200))
               .andExpect(jsonPath("$").isArray());
        verify(IRewardProxy).getProviders((UUID) any());
    }

    @Test
    void testGetTrackUserLocation_shouldReturnAVisitedLocationBean() throws Exception {
        // Arrange
        VisitedLocationBean locationBean = new VisitedLocationBean(
                UUID.fromString("7f4b5ac3-ed44-43b9-a17a-da414e5d888c"), new LocationBean(10.0, 10.0)
                , new Date(1L));
        VisitedLocationBean visitedLocation = new VisitedLocationBean(UUID.fromString("7f4b5ac3-ed44-43b9-a17a" +
                                                                                      "-da414e5d888c"),
                new LocationBean(10.0, 10.0)
                , new Date(1L));
        UserBean user = new UserBean(UUID.randomUUID(), "bandar", "0923789223", "even@nt.tp");
        user.addToVisitedLocations(visitedLocation);
        when(IGpsProxy.trackUserLocation((UUID) any())).thenReturn(locationBean);
        // Act and Assert
        mockMvc.perform(post("/trackUserLocation")
                       .contentType(String.valueOf(MediaType.APPLICATION_JSON))
                       .content(mapper.writeValueAsString(user)))
               .andExpect(status().is(200))
               .andExpect(jsonPath("$.userId", is("7f4b5ac3-ed44-43b9-a17a-da414e5d888c")));
        verify(IGpsProxy).trackUserLocation((UUID) any());
    }

    @Test
    void testGetTrackUserLocationAsync_shouldReturnAListOfVisitedLocationBean() throws Exception {
        // Arrange
        VisitedLocationBean locationBean = new VisitedLocationBean(
                UUID.fromString("7f4b5ac3-ed44-43b9-a17a-da414e5d888c"), new LocationBean(10.0, 10.0)
                , new Date(1L));
        List<VisitedLocationBean> visitedLocationBeans = Collections.singletonList(locationBean);
        when(IGpsProxy.trackUserLocationAsync(12)).thenReturn(visitedLocationBeans);
        // Act and Assert
        mockMvc.perform(get("/trackUserLocationAsync/{usersCount}", 12))
               .andExpect(status().is(200))
               .andExpect(jsonPath("$").isArray());
        verify(IGpsProxy).trackUserLocationAsync(12);
    }

    @Test
    void testStopTracking_shouldReturnA200HttpStatus() throws Exception {
        // Arrange
        doNothing().when(IGpsProxy).stopTracking();
        // Act and Assert
        mockMvc.perform(post("/stopTracking"))
               .andExpect(status().is(200));
        verify(IGpsProxy).stopTracking();
    }
}