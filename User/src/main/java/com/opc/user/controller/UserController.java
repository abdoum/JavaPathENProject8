package com.opc.user.controller;

import com.opc.user.exception.EntityNotFoundException;
import com.opc.user.exception.UserAlreadyExistException;
import com.opc.user.helper.InternalTestHelper;
import com.opc.user.model.Location;
import com.opc.user.model.User;
import com.opc.user.model.UserReward;
import com.opc.user.service.IUserService;
import com.opc.user.validator.Uuid;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Validated
@RestController
@Slf4j
@Api(value = "Users methods to add, retrieve a user and or its proprieties.")
public class UserController {


    /**
     * The User service.
     */
    @Autowired
    IUserService iUserService;

    @Autowired
    InternalTestHelper internalTestHelper;

    /**
     * Gets a user by id.
     *
     * @param userId the user id
     * @return user user
     * @throws EntityNotFoundException the entity not found exception
     */
    @ApiOperation(
            value = "Gets a user by id")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "A User object"),
                    @ApiResponse(code = 404, message = "In case the user is not found with the provided user id.")
            }
    )
    @GetMapping({"/users/{userId}"})
    public User getUser(@PathVariable("userId") @Uuid UUID userId) throws EntityNotFoundException {
        User user = iUserService.getUserById(userId);
        return user;
    }

    /**
     * Gets all users.
     *
     * @return all the users
     */
    @ApiOperation(
            value = "Get all existing users.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "A map of userId as key and user as value. Returns an empty " +
                                                       "map " +
                                                       "in case there are no users"),
            }
    )
    @GetMapping({"/users"})
    public Map<UUID, User> getAllUsers() {
        log.info("get all users request received.");
        var usersMap = iUserService.initializeInternalUsers();
        log.info(String.valueOf(usersMap.size()));
        return usersMap;
    }

    /**
     * Sets the internal user number to be generated.
     *
     * @param usersNumber the number of users
     * @return the new number of users that has been set.
     */
    @ApiOperation(
            value = "Sets the internal user number to be generated.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "the saved user number"),
                    @ApiResponse(code = 400, message = "In case the user is not found with the provided user id.")
            }
    )
    @PutMapping({"/users/count/{count}"})
    public int setUsersCount(@PathVariable("count") @Range(min = 0, max = 100_000) int usersNumber) {
        internalTestHelper.setUsersCount(usersNumber);
        return internalTestHelper.getUsersCount();
    }

    /**
     * Gets internal user number.
     *
     * @return the internal user number
     */
    @ApiOperation(
            value = "Gets the internal user number to be generated.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "the defined user number"),
                    @ApiResponse(code = 400, message = "In case the user is not found with the provided user id.")
            }
    )
    @GetMapping({"/users/count"})
    public int getUsersCount() {
        log.info(String.valueOf(internalTestHelper.getUsersCount()));
        return internalTestHelper.getUsersCount();
    }

    /**
     * Adds a user.
     *
     * @param user the added user
     * @throws UserAlreadyExistException the user already exist exception
     */
    @ApiOperation(
            value = "Saves a new user")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "the saved user number"),
                    @ApiResponse(code = 400, message = "In case the user with the provided user id already exists.")
            }
    )
    @PostMapping({"/users"})
    public void addUser(@Valid @RequestBody User user) throws UserAlreadyExistException {
        iUserService.saveUser(user);
    }

    /**
     * Gets all locations for all the users.
     *
     * @return a list of locations mapped by userId
     */
    @ApiOperation(
            value = "Gets the all locations for all the users.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "the list of users and their locations or an empty list " +
                                                       "if no locations were found"),
            }
    )
    @GetMapping({"/users/locations"})
    public Map<String, List<Location>> getAllUsersLocations() {
        return iUserService.getAllUsersLocations();
    }

    /**
     * Gets user rewards.
     *
     * @param userId UUID the user id
     * @return a list of the user rewards
     */
    @ApiOperation(
            value = "Gets user rewards for a specific user id.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "The list of user rewards")
            }
    )
    @GetMapping("/users/rewards/{userId}")
    public List<UserReward> getUserRewards(@PathVariable("userId") @Uuid UUID userId) {
        User user = iUserService.getInternalUserMap().get(userId);
        return iUserService.getUserRewards(user);
    }
}