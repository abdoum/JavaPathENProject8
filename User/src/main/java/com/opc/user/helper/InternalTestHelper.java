package com.opc.user.helper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * This class sets the number of users to generate for testing purposes.
 * The default value is 100.
 */
@Component
public class InternalTestHelper {

    /**
     * The number of users to generate for performance testing
     */
    private int usersCount;

    /**
     * Returns the number of users to be generated.
     *
     * @return int the number of users
     */
    public int getUsersCount() {
        return usersCount;
    }

    /**
     * Sets the internal user number to be generated. User this to change the number of users to be generated.
     * The default value is 100.
     *
     * @param usersCount the number of users to generate
     */
    @Value("${users.number}")
    public void setUsersCount(int usersCount) {
        this.usersCount = usersCount;
    }
}