package com.opc.user.model;

import org.javamoney.moneta.spi.MoneyUtils;

import javax.money.CurrencyUnit;
import java.math.BigDecimal;


public class Money {

    /**
     * Instantiates a new Money bean.
     *
     * @param number       the number
     * @param currencyCode the currency code
     */
    public Money(BigDecimal number, CurrencyUnit currencyCode) {
        this.monetaryContext = currencyCode;
        this.number          = number;
    }

    /**
     * Of money bean.
     *
     * @param number       the number
     * @param currencyCode the currency code
     * @return the money bean
     */
    public static Money of(Number number, CurrencyUnit currencyCode) {
        return new Money(MoneyUtils.getBigDecimal(number), currencyCode);
    }

    private final CurrencyUnit monetaryContext;

    private final BigDecimal number;

    @Override public String toString() {
        return "Money{" +
               "monetaryContext=" + monetaryContext +
               ", number=" + number +
               '}';
    }
}