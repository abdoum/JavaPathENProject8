package com.opc.user.model;


import java.util.Date;
import java.util.UUID;


public class VisitedLocation {

    /**
     * The User id.
     */
    public final UUID userId;

    /**
     * The Location.
     */
    public final Location location;

    /**
     * The Time visited.
     */
    public final Date timeVisited;

    /**
     * Instantiates a new Visited location bean.
     *
     * @param userId      the user id
     * @param location    the location
     * @param timeVisited the time visited
     */
    public VisitedLocation(UUID userId, Location location, Date timeVisited) {
        this.userId      = userId;
        this.location    = location;
        this.timeVisited = timeVisited;
    }
}