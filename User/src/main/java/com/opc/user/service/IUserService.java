package com.opc.user.service;

import com.opc.user.exception.EntityNotFoundException;
import com.opc.user.exception.UserAlreadyExistException;
import com.opc.user.model.Location;
import com.opc.user.model.User;
import com.opc.user.model.UserReward;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface IUserService {

    /**
     * Gets internal user map.
     *
     * @return the internal user map
     */
    Map<UUID, User> getInternalUserMap();

    /**
     * Gets user rewards.
     *
     * @param user the user
     * @return the user rewards
     */
    List<UserReward> getUserRewards(@NotNull User user);

    /**
     * Initialize internal users map.
     *
     * @return the map
     */
    Map<UUID, User> initializeInternalUsers();

    /**
     * Gets all users last location.
     *
     * @return the all users last location
     */
    Map<String, List<Location>> getAllUsersLocations();

    /**
     * Gets user by id.
     *
     * @param userId the user id
     * @return the user by id
     * @throws EntityNotFoundException the entity not found exception
     */
    User getUserById(UUID userId) throws EntityNotFoundException;

    /**
     * Save user user.
     *
     * @param user the user
     * @return the user
     * @throws UserAlreadyExistException the user already exist exception
     */
    User saveUser(User user) throws UserAlreadyExistException;
}