package com.opc.user.service;

import com.opc.user.exception.EntityNotFoundException;
import com.opc.user.exception.UserAlreadyExistException;
import com.opc.user.helper.InternalTestHelper;
import com.opc.user.model.Location;
import com.opc.user.model.User;
import com.opc.user.model.UserReward;
import com.opc.user.model.VisitedLocation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@Slf4j
public class UserService implements IUserService {

    private final Map<UUID, User> internalUserMap = new HashMap<>();

    @Autowired
    InternalTestHelper internalTestHelper;

    /**
     * Gets the list of in memory users.
     *
     * @return the internal user map
     */
    @Override public Map<UUID, User> getInternalUserMap() {
        return internalUserMap;
    }

    /**
     * Gets user rewards.
     *
     * @param user the user
     * @return the user rewards
     */
    @Override public List<UserReward> getUserRewards(@NotNull User user) {
        return user.getUserRewards();
    }

    /**
     * Initialize an internal users map.
     *
     * @return the generated users map
     */
    @Override public Map<UUID, User> initializeInternalUsers() {
        // clear existing internal users if any
        if (internalUserMap.size() > 0) {
            internalUserMap.clear();
        }
        IntStream.range(0, internalTestHelper.getUsersCount()).forEach((i) -> {
            String userName = "internalUser" + i;
            String phone = "000";
            String email = userName + "@tourGuide.com";
            UUID userId = UUID.randomUUID();
            User user = new User(userId, userName, phone, email);
            this.generateUserLocationHistory(user);
            this.internalUserMap.put(userId, user);
        });
        if (log.isDebugEnabled()) {
            log.debug(MessageFormat.format("Created {0} internal test users.",
                    internalTestHelper.getUsersCount()));
        }
        return this.internalUserMap;
    }

    /**
     * Generates a user location history.
     *
     * @param user the user
     */
    private void generateUserLocationHistory(User user) {
        IntStream.range(0, 3).forEach((i) -> {
            user.addToVisitedLocations(new VisitedLocation(user.getUserId(),
                    new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()));
        });
    }

    /**
     * Generates a random longitude double.
     *
     * @return the double
     */
    private double generateRandomLongitude() {
        double leftLimit = -180.0D;
        double rightLimit = 180.0D;
        return leftLimit + (new Random()).nextDouble() * (rightLimit - leftLimit);
    }

    /**
     * Generates a random latitude double.
     *
     * @return the double
     */
    private double generateRandomLatitude() {
        double leftLimit = -85.05112878D;
        double rightLimit = 85.05112878D;
        return leftLimit + (new Random()).nextDouble() * (rightLimit - leftLimit);
    }

    /**
     * Gets a random time.
     *
     * @return the random time
     */
    private Date getRandomTime() {
        LocalDateTime localDateTime = LocalDateTime.now().minusDays((new Random()).nextInt(30));
        return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
    }

    /**
     * Gets all users locations.
     *
     * @return the list of each user's locations
     */
    @Override public Map<String, List<Location>> getAllUsersLocations() {
        List<User> allUsers = new ArrayList<>(initializeInternalUsers().values());
        Map<String, List<Location>> response = new HashMap<>();
        allUsers.forEach(user -> {
                    UUID id = user.getUserId();
                    List<Location> locations = getVisitedLocationsCoordinates(user);
                    response.put(id.toString(), locations);
                }
        );
        return response;
    }

    /**
     * Returns the coordinates of all the visited locations for the user
     * @param user
     * @return A list of locations
     */
    private List<Location> getVisitedLocationsCoordinates(User user) {
        return user.getVisitedLocations().stream().map(visitedLocation -> visitedLocation.location).collect(Collectors.toList());
    }

    /**
     * Gets user by id.
     *
     * @param userId the user id
     * @return the user by id
     * @throws EntityNotFoundException if no user is found with the given id
     */
    @Override public User getUserById(UUID userId) throws EntityNotFoundException {
        User user = this.internalUserMap.get(userId);
        if (user == null) {
            throw new EntityNotFoundException("User with id " + userId + " could not be found");
        }
        return user;
    }

    /**
     * Save a user.
     *
     * @param user the user
     * @return the user
     * @throws UserAlreadyExistException the user already exist exception
     */
    @Override public User saveUser(User user) throws UserAlreadyExistException {
        var users = this.internalUserMap;
        if (!users.containsKey(user.getUserId())) {
            users.put(user.getUserId(), user);
        }
        throw new UserAlreadyExistException("user already exist");
    }
}