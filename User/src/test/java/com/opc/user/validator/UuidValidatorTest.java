package com.opc.user.validator;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UuidValidatorTest {

    @Test
    void testIsValid_shouldReturnFalse_forAnInvalidUUID() {
        UuidValidator uuidValidator = new UuidValidator();
        assertThrows(IllegalArgumentException.class, () -> uuidValidator.isValid(UUID.fromString("klnfiuwehf"), null));
    }

    @Test
    void testIsValid_shouldReturnTrue_forAValidUuid() {
        UuidValidator uuidValidator = new UuidValidator();
        assertTrue(uuidValidator.isValid(UUID.fromString("b90971ce-1fff-4dd9-8fd2-27285b3182d2"), null));
    }

}